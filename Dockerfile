FROM ubuntu:xenial

MAINTAINER soner sivri <sonersivri@gmail.com>

RUN set -x \
    && apt-get update --quiet \
        && apt-get install -y curl wget\
        && apt-get install --quiet --yes --no-install-recommends libtcnative-1 xmlstarlet \
        && apt-get install --quiet --yes software-properties-common python-software-properties \
        && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
        && add-apt-repository ppa:webupd8team/java \
        && apt-get update --quiet \
        && apt-get install -y oracle-java8-installer \
        && rm -rf /var/lib/apt/lists/* \
        && rm -rf /var/cache/oracle-jdk8-installer \
    && apt-get clean

# Use the default unprivileged account. This could be considered bad practice
# on systems where multiple processes end up being executed by 'daemon' but
# here we only ever run one process anyway.

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

CMD ["bash"]
